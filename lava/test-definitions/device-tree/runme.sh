#!/bin/sh

set +x

SYSFS_DEVICE_TREE="/sys/firmware/devicetree/base/"
CONFIG_PROC_FS=$(zcat /proc/config.gz | grep "CONFIG_PROC_FS=")
CONFIG_OF=$(zcat /proc/config.gz | grep "CONFIG_OF=")
[ "${CONFIG_PROC_FS}" = "CONFIG_PROC_FS=y" ] && [ "${CONFIG_OF}" = "CONFIG_OF=y" ] && [ -d "${SYSFS_DEVICE_TREE}" ]
status=$?
echo "<LAVA_SIGNAL_STARTTC device-tree-requirements>"
if [ "$status" -eq 0 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=device-tree-requirements RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=device-tree-requirements RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC device-tree-requirements>"

[ -n "$(cat /proc/device-tree/compatible)" ]
status=$?
echo "<LAVA_SIGNAL_STARTTC device-tree-compatible>"
if [ "$status" -eq 0 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=device-tree-compatible RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=device-tree-compatible RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC device-tree-compatible>"
