#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo system.tar.xz "${SYSTEM_URL}"
tar -xf system.tar.xz

adb shell mkdir -p /data/local/tmp/system/bin

for BUILD_SUFFIX in 64 c64; do
  adb push system/lib${BUILD_SUFFIX}/libssl.so \
    /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/
  adb push system/lib${BUILD_SUFFIX}/libcrypto.so \
    /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/
done


echo 'set -ex
cd /data/local/tmp
for BUILD_SUFFIX in 64 c64; do
  LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test \
    /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/boringssl_ssl_test
  LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test \
    /data/nativetest${BUILD_SUFFIX}/boringssl_crypto_test/boringssl_crypto_test
done
' > target-script-boringssl.sh
adb push target-script-boringssl.sh /data/local/tmp/
adb shell "sh /data/local/tmp/target-script-boringssl.sh" > /tmp/boringssl-log.txt 2>&1
result=$?
cat /tmp/boringssl-log.txt
if [ "$result" = "0" ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=boringssl RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=boringssl RESULT=fail>"
fi
