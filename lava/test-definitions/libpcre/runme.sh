#!/bin/sh

set +x

echo 'set -ex
for BUILD_SUFFIX in 64 c64; do
	cd /data/nativetest${BUILD_SUFFIX}/pcre2test
	srcdir=dist2 ./dist2/RunTest
	srcdir=dist2 ./dist2/RunGrepTest
done
' > target-script-pcre.sh
adb push target-script-pcre.sh /data/local/tmp/
adb shell "sh /data/local/tmp/target-script-pcre.sh" > /tmp/pcre-log.txt 2>&1
result=$?
cat /tmp/pcre-log.txt
if [ "$result" = "0" ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=pcre RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=pcre RESULT=fail>"
fi
