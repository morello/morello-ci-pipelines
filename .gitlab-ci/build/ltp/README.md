# Build script for Linux Test Project (LTP) for Morello

## Setup requirements
Any fairly recent Linux-based distribution (x86_64 based) with
basic dev tools should do.\
Packages required:

 GNU make, GNU C compiler for aarch64, git, autoconf, automake, m4,
 pkgconf / pkg-config

## Building

As simple as running:

```
./build_ltp.sh
```

Build artifacts can be found at:
```
./playground/out
```
LTP being installed under:
```
./playground/out/opt
```
\* See section below on how to modify the paths specified above

### Details

The build script itself relies on ***config***  and ***utils*** file being
available from within the same location as the script itself.\
The first one specifies configuration for the build (@see details below), with
the latter providing set of helper functions.

Required entries for ***config*** file:

- *PLAYGROUND* : path to the location where build sources are to be stored
(top directory)
- *OUTPUT_DIR* : path to the location where build artifacts are to be stored
- configuration for each of the required software components in a form of
an array consisting of the following:

  - *identifier*    : any name to identify the project, used for creating
folder hierarchy for both sources and artifacts
  - *repository URL*: where to fetch the sources/blobs from
  - *branch name*   : either branch or tag name to fetch from

  e.g:

      (name                       url 	                                   branch      )
  	("ltp" "https://git.morello-project.org/morello/morello-linux-ltp.git" "morello/next")

Optional:

- *DEFAULT_REMOTE*=morello : the script uses $DEFAULT_REMOTE to specify
the name for each remote outlined int the ***config*** file


#### Currently supported config:

```
PLAYGROUND="$(pwd)/playground"
OUTPUT_DIR="${PLAYGROUND}/out"

MORELLO_COMPILER_VERSION=1.4

# Setup = (label repository_url branch)
LLVM_SETUP=("llvm" "https://git.morello-project.org/morello/llvm-project-releases.git" "morello/linux-aarch64-release-1.4")
MUSL_SETUP=("musl" "https://git.morello-project.org/morello/musl-libc.git" "tags/morello-release-1.4.0")
LINUX_SETUP=("linux_headers" "https://git.morello-project.org/morello/morello-linux-headers.git" "morello/master")
LTP_SETUP=("ltp" "https://git.morello-project.org/morello/morello-linux-ltp.git" "morello/next")

DEFAULT_REMOTE=morello
```

### Build steps:
- ***LLVM setup***: fetching specified LLVM release for aarch64 toget to get
a hold of CRT objects and compiler-rt for aarch64-linux-musl_purecap target;
switching then back to x86_64-unknown-linux-gnu target release to compile musl & LTP
- ***Musl setup***: fetching sources and building for aarch64-linux-musl_purecap
target with libshim disabled
	(@see https://git.morello-project.org/morello/musl-libc/-/blob/morello/master/README.rst for more details)
- ***Linux kernel uAPI headers***: fetching stable version of the headers
from provided repository (to be used by LTP)
- ***LTP setup***: fetching sources for Morello LTP and creating static binaries
linked against previously built musl\
	note: the fork contains changes necessary to build LTP in purecap mode,
	plus number of improvements to LTP build script that are leveraged by
	this script itself; currently supporting only 'testcases/kernel/syscalls'
	tests

### Artifacts

The artifacts are being stored within "${OUTPUT_DIR}" location. Each component
receives a dedicated sub-directory, with a name corresponding to the one
provided within the ***config*** file.
Additionally "${OUTPUT_DIR}"/opt/ltp directory will be created for installing
LTP framework and tests



#### Notes:
- The actual commands are being executed under new shell so host environment
variables should not be affected.
- The script does not perform any clean-up on it's own.
- All repositories are based on git remotes named as "${DEFAULT_REMOTE}"
  which will trigger issues when switching those in the ***config***
  file unless "${DEFAULT_REMOTE}" gets changed as well.
- All repositories are shallow clones.

#### LTP setup requirements (the list might not be complete):
- procfs mounted (under standard '/proc' mount-point)
- shmfs mounted (under standard '/dev/shm' mount-point, min 256MB)
- /tmp is not mounted as tmpfs
- /etc/passwd & /etc/groups are present containing entries for:
	- *root*, *nobody*, *bin*, *deamon*  user ids and groups
	- *users*, *sys* groups



#### Running LTP tests for Morello:

Assuming LTP has been installed under /opt/ltp (default location)
```
/opt/ltp/runltp -f /opt/ltp/runtest/morello_transitional,/opt/ltp/runtest/morello_transitional_extended

```
