#!/usr/bin/env bash

sudo apt update
sudo apt install -y automake

# delete artifacts from previous stage
rm -rf *
git clone https://git.morello-project.org/morello/morello-pcuabi-env -b $CI_COMMIT_REF_NAME

cd morello-pcuabi-env/morello

# Detect Architecture
_MORELLO_ARCH=$(uname -m)

if [ "${_MORELLO_ARCH}" == 'x86_64' ]; then
	MORELLO_ARCH="--x86_64"
elif [ "${_MORELLO_ARCH}" == 'aarch64' ]; then
	MORELLO_ARCH="--aarch64"
else
	echo "Architecture not supported!"
fi

# Build morello-pcuabi-env
source env/morello-pcuabi-env
scripts/build-all.sh \
	${MORELLO_ARCH} \
	--firmware \
	--linux \
	--kselftest \
	--c-apps \
	--rootfs \
	--docker
