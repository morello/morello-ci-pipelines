#!/usr/bin/env python3
#
# This script builds and runs musl-libc/test/src tests.

import argparse
import json
import os
import platform
import re
from argparse import Namespace
from dataclasses import dataclass
from pathlib import Path
from typing import Any, List

import utils
from utils import (
    AnsiColors,
    ExecutionInfo,
    ExpectedFailures,
    Log,
    ParallelExecution,
    Remarks,
    Result,
    TemporaryResources,
)


#  List library dependencies for tests.
LibraryDependencies = {
    'shared_third_party_library': ['third-party-library'],
    'symbol_third_party_library': ['symbol-library'],
    'multiple_third_party_libraries': ['multi-lib1', 'multi-lib2'],
    'multiple_symbol_third_party_library': ['symbol-library', 'third-party-library'],
    'third_party_library_dependency_library': ['dependency-lib1', 'dependency-lib2'],
    'pointer_third_party_library': ['third-party-pointer-lib'],
    'variadic_argument_third_party_library': ['variadic-lib'],
}


class Assets:
    '''Helper to access test assets'''

    def __init__(self, args: Namespace) -> None:
        self.args = args

    @property
    def c_compiler_path(self) -> Path:
        return self.compiler_bin_path / 'clang'

    @property
    def build_output_path(self) -> Path:
        return self.output_path / 'build'

    @property
    def build_lib_output_path(self) -> Path:
        return self.build_output_path / 'lib'

    @property
    def build_resources_output_path(self) -> Path:
        return self.build_output_path / 'resources'

    @property
    def musl_libc_test_path(self) -> Path:
        return self.musl_libc_path / 'test'

    @property
    def musl_libc_test_lib_path(self) -> Path:
        return self.musl_libc_test_path / 'src' / 'lib'

    @property
    def musl_libc_test_resources_path(self) -> Path:
        return self.musl_libc_test_path / 'src' / 'resources'

    @property
    def junit_xml_path(self) -> Path:
        return self.output_path / 'musl_libc_tests_junit_report.xml'

    def __getattr__(self, name: str) -> Any:
        return self.args.__getattribute__(name)


@dataclass
class TestDescription:
    '''Description of a single test'''

    name: str
    args: str
    env: dict
    test_file: dict
    source_path: Path
    output_path: Path
    stdin: str
    expected_returncode: int
    expected_stdout: str
    expected_stderr: str
    dynamic: bool

    def __lt__(self, other):
        return self.name < other.name


def load_test_descriptions(assets: Assets, description: Path) -> list:
    '''Loads test descriptions from a json file'''
    Log.pretty().write('Loading test descriptions from ').highlight(description).flush()
    with open(description, 'r') as r_file:
        test_descs = json.load(r_file)
    tests = []
    for test_desc in test_descs:
        filename = (
            test_desc['app']
            .replace('${build}/', '')
            .replace('-${kind}', '')
            .replace('.exe', '')
        )
        args = test_desc.get('args', [])
        name = test_desc.get('name', f'{filename}_{"_".join(args)}')
        test_description = TestDescription(
            name=name,
            args=args,
            env=test_desc.get('env', {}),
            test_file=test_desc.get('file', {}),
            source_path=assets.musl_libc_path / f'test/src/{filename}.c',
            output_path=assets.build_output_path / name,
            stdin=test_desc.get('stdin', None),
            # The test description lists two exit codes for a crash.
            # We only want the second one.
            expected_returncode=test_desc.get('xrc', [0])[-1],
            expected_stdout=test_desc.get('stdout', None),
            expected_stderr=test_desc.get('stderr', None),
            dynamic=assets.dynamic,
        )
        tests.append(test_description)
    tests.sort()
    return tests


def match_expected_outputs(expected: List[str], actual: List[str]) -> Result:
    '''Checks that an expected output matches the actual one'''

    def failed() -> Result:
        msg = [
            '',
            Log.format(AnsiColors.RED, 'Expected output is different from actual.'),
            Log.format(AnsiColors.GREEN, 'Expected >>>'),
            Log.format(AnsiColors.WHITE, '\n'.join(expected)),
            Log.format(AnsiColors.GREEN, '<<<'),
            Log.format(AnsiColors.RED, 'Actual >>>'),
            Log.format(AnsiColors.WHITE, '\n'.join(actual)),
            Log.format(AnsiColors.RED, '<<<'),
        ]
        return Result.fail(value='\n'.join(msg))

    if len(expected) != len(actual):
        return failed()

    for exp, act in zip(expected, actual):
        if not re.fullmatch(exp, act) and exp != act:
            return failed()

    return Result.success()


def match_expected_returncodes(expected: int, actual: int) -> Result:
    '''Checks that an expected returncode matches the actual one'''

    def failed() -> Result:
        msg = [
            '',
            Log.format(AnsiColors.RED, 'Expected exit code is different from actual.'),
            Log.format(AnsiColors.GREEN, 'Expected'),
            Log.format(AnsiColors.WHITE, expected),
            Log.format(AnsiColors.RED, 'Actual'),
            Log.format(AnsiColors.WHITE, actual),
        ]
        return Result.fail(value='\n'.join(msg))

    if expected != actual:
        return failed()
    return Result.success()


def get_build_execution_info(
    assets: Assets,
    name: str,
    is_library: bool,
    source_path: Path,
    output_path: Path,
    data: Any = None,
    executed_cb: Any = None,
) -> ExecutionInfo:
    '''Creates an ExecutionInfo for a build'''
    c_flags = [
        f'--sysroot={assets.musl_libc_sysroot_path}',
        f'--target={assets.target_triple}',
    ]

    if assets.dynamic:
        c_flags.append('-fPIC')
        c_flags.append('-DDYNAMIC')
        if is_library:
            c_flags.append('-shared')
    else:
        c_flags.append('-static')
        if is_library:
            c_flags.append('-c')

    c_flags.append(f'-I{assets.musl_libc_test_lib_path}')

    if assets.dynamic or (not assets.dynamic and not is_library):
        c_flags.append('-lc')
        c_flags.append('-lpthread')
        c_flags.append('-lm')
        c_flags.append('-lrt')
        c_flags.append('-lutil')
        c_flags.append('--rtlib=compiler-rt')
        c_flags.append('-fuse-ld=lld')

    if assets.enable_cheriseed:
        c_flags.append('-fsanitize=cheriseed')
        c_flags.append('-mabi=purecap')
        c_flags.append('-D__SANITIZE_CHERISEED__')
        c_flags.append('-DLIBSHIM')
    else:
        c_flags.append('-fsanitize=')

    if assets.dynamic and assets.enable_cheriseed and not is_library:
        c_flags.append('-shared-libsan')
        c_flags.append(
            f'-Wl,--dynamic-linker,{assets.musl_libc_sysroot_path}/lib/libc.so'
        )

    if assets.verbose:
        c_flags.append('-g')

    for lib_dep in LibraryDependencies.get(name, []):
        extension = '.so' if assets.dynamic else '.o'
        dep = str(assets.build_lib_output_path / f'{lib_dep}{extension}')
        c_flags.append(dep)

    command = [
        str(assets.c_compiler_path),
        *c_flags,
        str(source_path),
        '-o',
        str(output_path),
    ]

    return ExecutionInfo(
        name=name,
        category='build.dynamic' if assets.dynamic else 'build.static',
        command=command,
        timeout=assets.timeout,
        data=data,
        cwd=assets.build_output_path,
        verbose=assets.verbose,
        executed_cb=executed_cb,
    )


def get_build_execution_info_for_desc(
    assets: Assets, desc: TestDescription
) -> ExecutionInfo:
    '''Creates an ExecutionInfo from a TestDescription'''
    return get_build_execution_info(
        assets,
        desc.name,
        False,
        desc.source_path,
        desc.output_path,
        desc,
        get_run_execution_info,
    )


def get_build_execution_info_for_library(
    assets: Assets, filepath: Path
) -> ExecutionInfo:
    '''Creates an ExecutionInfo for a library build'''
    output_path = assets.build_lib_output_path / filepath.stem
    if assets.dynamic:
        output_path = output_path.with_suffix('.so')
    else:
        output_path = output_path.with_suffix('.o')
    return get_build_execution_info(assets, filepath.stem, True, filepath, output_path)


def get_run_execution_info(exec_info: ExecutionInfo) -> ExecutionInfo:
    '''Creates an ExecutionInfo from another ExecutionInfo'''
    desc: TestDescription = exec_info.data
    command = [str(desc.output_path), *desc.args]
    env = os.environ.copy()
    for env_var, value in desc.env.items():
        env[env_var] = value

    return ExecutionInfo(
        name=desc.name,
        category='execute.dynamic' if desc.dynamic else 'execute.static',
        command=command,
        env=env,
        cwd=exec_info.cwd,
        check=False,
        timeout=exec_info.timeout,
        stdin=desc.stdin,
        verbose=exec_info.verbose,
        data=desc,
        executed_cb=evaluate_run,
        # Skip running but show it in the output for consistency
        result=Result.fail() if not exec_info.result.is_success() else None,
    )


def evaluate_run(exec_info: ExecutionInfo) -> None:
    '''Evaluates the execution, regarding stdout, stderr and returncode'''
    if not exec_info.result.is_success():
        return None

    # Compare expected and actual stdout and stderr
    desc: TestDescription = exec_info.data

    # FIXME: support test.file
    exec_info.result = match_expected_returncodes(
        desc.expected_returncode, exec_info.returncode
    )
    if not exec_info.result.is_success():
        exec_info.stderr += exec_info.result.value
        return None

    if desc.expected_stdout and exec_info.stdout:
        exec_info.result = match_expected_outputs(
            desc.expected_stdout, exec_info.stdout.split('\n')
        )
        if not exec_info.result.is_success():
            exec_info.stderr += exec_info.result.value
            return None

    if desc.expected_stderr and exec_info.stderr:
        exec_info.result = match_expected_outputs(
            desc.expected_stderr, exec_info.stderr.split('\n')
        )
        if not exec_info.result.is_success():
            exec_info.stderr += exec_info.result.value
            return None

    return None


def parse_arguments() -> Namespace:
    '''Parses program arguments'''
    parser = argparse.ArgumentParser(
        description='Runs musl-libc tests',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '--output-path',
        dest='output_path',
        action='store',
        type=Path,
        default=utils.env_root_path('scratch/musl-libc/test'),
        help='output path to store the results',
    )
    parser.add_argument(
        '--musl-libc-path',
        dest='musl_libc_path',
        action='store',
        type=Path,
        default=utils.env_root_path('musl-libc'),
        help='path to musl-libc source code',
    )
    parser.add_argument(
        '--musl-libc-sysroot-path',
        dest='musl_libc_sysroot_path',
        action='store',
        type=Path,
        default=utils.env_root_path(
            f'musl-libc/obj/install/{platform.machine()}-linux-musl'
        ),
        help='path to musl-libc sysroot',
    )
    parser.add_argument(
        '--compiler-bin-path',
        dest='compiler_bin_path',
        action='store',
        type=Path,
        default=utils.env_root_path('llvm-project/build/bin'),
        help='path to the compiler binaries',
    )
    parser.add_argument(
        '--target-triple',
        dest='target_triple',
        action='store',
        type=str,
        default=f'{platform.machine()}-linux-musl',
        help='target triple',
    )
    parser.add_argument(
        '--enable-cheriseed',
        dest='enable_cheriseed',
        action='store_true',
        help='enables CHERIseed',
    )
    parser.add_argument(
        '--filter',
        dest='filter',
        action='store',
        type=str,
        default=None,
        help='selects tests based on a python regex pattern',
    )
    parser.add_argument(
        '--dynamic', dest='dynamic', action='store_true', help='test dynamic binaries'
    )
    utils.add_usual_arguments(parser, 'musl.libc.test', 60.0)
    return parser.parse_args()


def main() -> Result:
    '''The main of the program'''
    assets = Assets(parse_arguments())
    utils.create_empty_directory(assets.output_path)
    # Load test descriptions from the JSON file.
    test_descriptions = load_test_descriptions(
        assets, assets.musl_libc_test_path / 'morello-tests.json'
    )
    # List managed resources.
    resources = [
        assets.build_output_path,
        assets.build_lib_output_path,
        Path(f'/tmp/{os.getenv("USER", "morello-tests")}'),
    ]
    # Add test files to the managed resources.
    for test_description in test_descriptions:
        if test_description.test_file:
            resources.append(
                TemporaryResources.File(Path(test_description.test_file['path']))
            )
    # Add test reources
    resources.append(
        TemporaryResources.Directory(
            assets.build_resources_output_path, assets.musl_libc_test_resources_path
        )
    )
    # Clean up before and after tests are executed.
    with TemporaryResources(resources, cleanup=assets.no_cleanup):
        with Remarks() as remarks:
            expected_failures = ExpectedFailures.parse_from_file(
                assets.expected_failures_path, remarks
            )

            pool = ParallelExecution(
                assets.nproc,
                testsuite_name=assets.testsuite_name,
                error_threshold=assets.error_threshold,
                expected_failures=expected_failures,
            )

            # Build libraries
            pool.execute(
                [
                    get_build_execution_info_for_library(assets, filepath)
                    for filepath in assets.musl_libc_test_lib_path.glob("*.c")
                ]
            )

            if assets.filter:
                pattern = re.compile(assets.filter)
                test_descriptions = [
                    test_description
                    for test_description in test_descriptions
                    if pattern.fullmatch(test_description.name)
                ]

            # Create test files.
            for test_description in test_descriptions:
                if test_description.test_file:
                    with open(test_description.test_file['path'], 'w') as w_file:
                        w_file.writelines(test_description.test_file['contents'])
            pool.execute(
                [
                    get_build_execution_info_for_desc(assets, test_desc)
                    for test_desc in test_descriptions
                ]
            )

            return pool.summarize(
                junit_xml_path=assets.junit_xml_path,
                allow_unstable=assets.allow_unstable,
            )


if __name__ == '__main__':
    utils.main_wrapper(main)
