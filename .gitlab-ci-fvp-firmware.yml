workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_LABELS =~ /^.*ci skip.*/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH =~ /^morello\/\S+/
      when: always

stages:
  - fetch
  - test

fetch-busybox:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: fetch
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - curl -L -o fvp-firmware.zip https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-busybox
    - unzip -n fvp-firmware.zip
    - rm -rf fvp-firmware.zip
    - echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
  artifacts:
    paths:
      - "busybox.img.xz"
      - "*.bin"
    expire_in: 5 days
    reports:
      dotenv: build.env


fetch-android-nano:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: fetch
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - curl -L -o fvp-firmware.zip https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-android-nano
    - unzip -n fvp-firmware.zip
    - rm -rf fvp-firmware.zip
    - echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
  artifacts:
    paths:
      - "android-nano.img.xz"
      - "*.bin"
    expire_in: 5 days
    reports:
      dotenv: build.env

fetch-morello-rootfs-images-fvp:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: fetch
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - curl -L -o morello-fvp.tar.xz https://git.morello-project.org/morello/morello-rootfs-images/-/jobs/artifacts/morello/mainline/raw/morello-fvp.tar.xz?job=build-morello-rootfs-images
    - curl -L -o fvp-firmware.zip https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-firmware
    - unzip -n fvp-firmware.zip
    - rm -rf fvp-firmware.zip
    - tar -xf morello-fvp.tar.xz --strip-components=1
    - xz -T0 morello-fvp.img
    - cp ${CI_PROJECT_DIR}/output/fvp/firmware/*.bin ${CI_PROJECT_DIR}
    - echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
  artifacts:
    paths:
      - "morello-fvp.img.xz"
      - "*.bin"
    expire_in: 5 days
    reports:
      dotenv: build.env

test-tuxsuite-busybox:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: busybox
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: fetch-busybox
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"


test-tuxsuite-android-nano-boot:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: android-nano-boot
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: fetch-android-nano
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-tuxsuite-boot-busybox-dt:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: boot-busybox-dt
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: fetch-morello-rootfs-images-fvp
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-tuxsuite-boot-debian-dt:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: boot-debian-dt
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: fetch-morello-rootfs-images-fvp
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-tuxsuite-boot-busybox-acpi:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: boot-busybox-acpi
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: fetch-morello-rootfs-images-fvp
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-tuxsuite-boot-debian-acpi:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: boot-debian-acpi
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: fetch-morello-rootfs-images-fvp
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"
